<?php // calc.php
  // Receive variables initialization.
  $time = $vel = 0;
  $scale = $unit = '';
  $time_objective = 0;
  $out_string = '';
  
  // Get shit from $_POST array.
  if (isset($_POST['velocity'])) $vel = floatval($_POST['velocity']);
  if (isset($_POST['time'])) $time = floatval($_POST['time']);
  if (isset($_POST['scale'])) $scale = $_POST['scale'];
  if (isset($_POST['unit'])) $unit = $_POST['unit'];
  
  // If timescale or velocity units are non-standard - make them standard.
  if ($scale != '') $time = make_seconds($time, $scale, 0);
  if ($unit != '') $vel = make_meters_per_second($vel, $unit);
  
  // Calculate objective time and revert it back to the user chosen timescale.
  if ($time < 0 || $vel < 0) $out_string = "Oops! You messed up with some numbers!";
  elseif ($vel > 299792458) $out_string = "Sorry, but you can't go faster than light.";
  else $time_objective = make_seconds(get_dilation($time, $vel), $scale, 1);
  
  $template_html = file_get_contents('template.html');
  
  if ($time_objective != 0)
  {
    $out_string = "$time_objective $scale of 'outer' time passed.";
  }
  
  // Show page.
  $output_html = sprintf($template_html, $out_string);
  echo $output_html;
  
  /**
   * Function that calculates how much of objective time passed if the
   * observer mooves with given speed given amount of time. Physics and shit.
   * 
   * @param float $time_sec Time, must be converted to seconds.
   * 
   * @param float $vel_ms Velocity, must be converted to meters per second.
   *
   * @returns float
   *
   */
  function get_dilation($time_sec, $vel_ms)
  {
    $c = 299792458.0; // Speed of light in vacuum.
    $time_sec = $time_sec / sqrt(1.0 - (pow($vel_ms, 2) / pow($c, 2)));
    return $time_sec;
  }
  
  /**
   * Function that convert $time from given timescale to seconds.
   * Can be switched to reverse mode.
   * 
   * @param float $time Time in any given timescale.
   * 
   * @param float $scale Timescale set by the user.
   *
   * @param int $revert Flag. 0 - if we need to convert to seconds.
   * 1 - if we need to convert from seconds to any given timescale.
   *
   * @returns float
   *
   */
  function make_seconds($time, $scale, $revert)
  {
    $coef = 1.0;
    
    if ($scale == "minutes") $coef = 60;
    elseif ($scale == "hours") $coef = 3600;
    elseif ($scale == "days") $coef = 3600 * 24;
    elseif ($scale == "months") $coef = 3600 * 24 * 30; // Let's assume all months have 30 days.
    elseif ($scale == "years") $coef = 3600 * 24 * 30 * 12;
    
    if ($revert) $coef = 1.0 / $coef;
    
    return ($time * $coef);
  }
  
  /**
   * Function that convert $vel from any given units to meters per second.
   * 
   * @param float $vel Velocity in any given units.
   * 
   * @param float $unit Velocity measurment unit set by the user.
   *
   * @returns float
   *
   */
  function make_meters_per_second($vel, $unit)
  {
    if ($unit == "kms") $vel = $vel * 1000;
    elseif ($unit == "kmh") $vel = $vel * 1000.0 / 3600.0;
    return $vel;
  }
?>
