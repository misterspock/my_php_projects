<?php // main.php
  // Main chat interface module.
?>
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>Spock's Chat Room</title>
    <link type='text/css' rel='stylesheet' href='css/styles.css'>
    <script type='text/javascript' src='js/OSC.js'></script>
    <script type='text/javascript' src='js/ajax.js'></script>
  </head>
  <body>
    <section>
      <h3>Mister Spock's Chat room</h3>
      <div id='chatWindow'>
      </div>
      <textarea id='message' name='message' rows='2'></textarea>
      <button onclick='writeIntoLog()'>Send</button>
    </section>
  </body>
</html>
