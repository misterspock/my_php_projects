<?php // loginform.php
  // User log-in form template file.
?>
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>Login screen</title>
    <link type='text/css' rel='stylesheet' href='css/styles.css'>
  </head>
  <body>
    <section>
      <h3>Log in into Chat</h3>
      <form action='core/login.php' method='POST'>
        <label>Enter Your nickname: </label>
        <input type='text' maxlength='32' name='nickname'>
        <input type='submit' value="Log In">
      </form>
    </section>
  </body>
</html>
