// AJAX handler functions.

document.onload = setInterval(readFromLog, 2500);

function writeIntoLog()
{
  // Get the message and clear textarea.
  var message = O('message').value;
  O('message').value = '';
  
  // Build AJAX request and send it.
  var nocache = '&nocache=' + Math.random() * 1000000;
  var params  = '?action=logwrite&message=' + message;
  var request = new XMLHttpRequest();
  
  request.open('GET', 'core/logaccess.php' + params + nocache, true);
  
  request.onreadystatechange = function()
  {
    if (this.readyState == 4)
    {
      if (this.status == 200)
      {
        if (this.responseText != null)
        {
          O('chatWindow').innerHTML = this.responseText;
        }
        else alert('AJAX error: no data received');
      }
      else alert('AJAX error: ' + this.statusText);
    }
  }
  request.send(null);
}

function readFromLog()
{
  var nocache = '&nocache=' + Math.random() * 1000000;
  var params  = '?action=logread';
  var request = new XMLHttpRequest();
  
  request.open('GET', 'core/logaccess.php' + params + nocache, true);
  
  request.onreadystatechange = function()
  {
    if (this.readyState == 4)
    {
      if (this.status == 200)
      {
        if (this.responseText != null)
        {
          O('chatWindow').innerHTML = this.responseText;
        }
        else alert('AJAX error: no data received');
      }
      else alert('AJAX error: ' + this.statusText);
    }
  }
  request.send(null);
}
