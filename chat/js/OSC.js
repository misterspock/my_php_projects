/* Three functions to acces the DOM will save me some typing. */
function O(i)
{
  return typeof i == 'object' ? i : document.getElementById(i);
}

function S(i)
{
  return O(i).style;
}

function C(i)
{
  return document.getElementsByClassName(i);
}
