<?php // index.php
  // App entry point.
  
  ini_set('session.gc_maxlifetime', 60 * 60 * 24);
  ini_set('display_errors', 1);
  session_start();
  
  if (isset($_SESSION['nickname']))
  {
    // Start chat interface.
    include 'templates/main.php';
  }
  else
  {
    // Load login form.
    include 'core/login.php';
  }
