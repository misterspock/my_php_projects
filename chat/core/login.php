<?php // login.php
  // User log-in handler.
  
  $nickname = $color = '';
  
  if (isset($_POST['nickname']))
  {
    // Put user nickname into session array and redirect user to 'index.php'.
    $nickname = sanitize($_POST['nickname']);
    $color    = '#' . strtoupper(dechex(mt_rand(0, 16777215)));
    
    session_start();
    $_SESSION['nickname'] = $nickname;
    $_SESSION['color']    = $color;
    
    header('Location: /chat/index.php');
    die();
  }
  else
  {
    // Display login form.
    include 'templates/loginform.php';
  }
  
  // Sanitizes string.
  function sanitize($str)
  {
    $str = stripslashes($str);
    $str = htmlEntities($str, ENT_QUOTES);
    return $str;
  }
