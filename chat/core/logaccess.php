<?php // logaccess.php
  // Chat log file read and write module.
  
  // Implement simple routing
  $action = '';
  $action = $_GET['action'];
  
  switch ($action)
  {
    case 'logread':
      // Read chat log.
      echo read_log();
      break;
      
    case 'logwrite':
      // Write chat log.
      // Get access to the session data.
      session_start();
      
      // Get message from user.
      $message = sanitize($_GET['message']);
      
      if (isset($_SESSION['nickname']))
      {
        // Extract session data.
        $nickname = $_SESSION['nickname'];
        $color    = $_SESSION['color'];
        
        // Construct a message to write into logfile.
        $message = "<p><span style='color: $color'>$nickname</span>: $message</p>";
        
        write_log($message);
        echo read_log();
      }
      break;
  }
  
  // Function to return contents of a logfile.
  function read_log()
  {
    $log = file_get_contents('../chatlog/chatlog.html');
    return $log;
  }
  
  // Function to write a message to the logfile.
  function write_log($message)
  {
    // Open logfile, lock it and write message.
    $fh = fopen('../chatlog/chatlog.html', 'cb') or die('Unable to open file!');
    if (flock($fh, LOCK_EX))
    {
      fseek($fh, 0, SEEK_END);
      fwrite($fh, $message . "\n") or die('Unable to write into file!');
      flock($fh, LOCK_UN);
      fclose($fh);
    }
  }
  
  // Sanitizes string.
  function sanitize($str)
  {
    $str = stripslashes($str);
    $str = htmlEntities($str, ENT_QUOTES);
    return $str;
  }
