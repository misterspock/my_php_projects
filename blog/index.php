<?php //index.php
  // App entry point.
  require_once 'core/model.php';

  ini_set('display_errors', 1);
  ini_set('session.gc_maxlifetime', 3600 * 4);
  
  $action = 'main'; // Default action.

  // Taking requested action.
  if (isset($_GET['action']))  $action = sanitize($_GET['action']);
  if (isset($_POST['action'])) $action = sanitize($_POST['action']);

  // Starting session.
  session_start();

  // Checking for started session.
  if (isset($_SESSION['admin']) &&
      isset($_SESSION['passwd']) &&
      ($_SESSION['admin'] == ADMIN) &&
      ($_SESSION['passwd'] == PASSWD))
  {
    $logged_in = true;
  }
  else
  {
    $logged_in = false;
  }
  
  // Routing requests.
  switch ($action)
  {
    case 'main':
      get_all();
      break;
      
    case 'viewpost':
      if (isset($_GET['id']))
      {
        $id = sanitize($_GET['id']);
        get_by_id($id);
      }
      else
      {
        generate_404("No set id were found in the request.");
      }
      break;

    case 'addpost':
      if (isset($_POST['title']) &&
          isset($_POST['summary']) &&
          isset($_POST['content']))
      {
        $title   = sanitize($_POST['title']);
        $summary = sanitize($_POST['summary']);
        $content = sanitize($_POST['content']);
        add_or_update(null, $title, $summary, $content);
      }
      else if (!isset($_POST['title']) ||
               !isset($_POST['summary']) ||
               !isset($_POST['content']))
      {
        if ($logged_in) include 'templates/newpost.php';
        else generate_404("Not allowed here. Please log in.");
      }
      break;

    case 'edit':
      if (isset($_GET['id']))
      {
        $id = sanitize($_GET['id']);
        
        if ($logged_in) get_by_id($id, true);
        else generate_404("You are not allowed here. Please, log in.");
      }
      else generate_404("No set id were found in the request.");
      break;

    case 'editpost':
      if (isset($_POST['id']) &&
          isset($_POST['title']) &&
          isset($_POST['summary']) &&
          isset($_POST['content']))
      {
        $id      = sanitize($_POST['id']);
        $title   = sanitize($_POST['title']);
        $summary = sanitize($_POST['summary']);
        $content = sanitize($_POST['content']);
        add_or_update($id, $title, $summary, $content, true);
      }
      else generate_404("Something REALLY bad happened!");
      break;
      
    case 'delete':
      if (isset($_GET['id']))
      {
        $id = sanitize($_GET['id']);
        if ($logged_in) get_by_id($id, false, true);
        else generate_404("You are not allowed here. Please, log in.");
      }
      else generate_404("No set id were found in the request.");
      break;

    case 'login':
      if (!$logged_in)
      {
        if (isset($_POST['name']) &&
            isset($_POST['password']))
        {
          $name     = sanitize($_POST['name']);
          $password = sanitize($_POST['password']);
          if ($name == ADMIN && $password == PASSWD)
          {
            $_SESSION['admin']  = $name;
            $_SESSION['passwd'] = $password;
            header("Location: index.php");
            die();
          }
          else generate_404("Wrong login and password!");
        }
        else include 'templates/login_form.php';
      }
      else generate_404("You are already logged in.");
      break;

    case 'logout':
      if ($logged_in)
      {
        $_SESSION = array();
        session_destroy();
        header("Location: index.php");
        die();
      }
      else generate_404("You have to be logged in to do this!");
      
    default:
      generate_404();
      break;
  }

  // Function to sanitize input.
  function sanitize($string)
  {
    $string = stripslashes($string);
    // $string = strip_tags($string);
    $string = htmlEntities($string, ENT_QUOTES);
    return $string;
  }
