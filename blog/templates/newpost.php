<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>Add new post</title>
    <link type='text/css' rel='stylesheet' href='css/styles.css'>
  </head>
  <body>
    <header>
      <h1>Spock's blog</h1>
    </header>
    <section>
      <article>
        <form action='index.php' method='POST'>
          <input type='hidden' name='action' value='addpost'>
          
          <div class='tableRow'>
            <label>Title:</label>
            <input type='text' name='title' maxlength='255' required='requred'>
          </div>
          <div class='tableRow'>
            <label>Summary info:</label>
            <textarea name='summary' rows='5' required='requred'></textarea>
          </div>
          <div class='tableRow'>
            <label>Content:</label>
            <textarea name='content' rows='15' required='requred'></textarea>
          </div>
          
          <input type='submit' value="Add post">
        </form>
      </article>
    </section>
    <footer>
      
    </footer>
  </body>
</html>
