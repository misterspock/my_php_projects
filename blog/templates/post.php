<?php
  $result->data_seek(0);
  $row = $result->fetch_array(MYSQLI_ASSOC);
  extract($row);
  $content = nl2br($content);  // This will translate all newline characters into HTML tags.
  $result->close();
  $conn->close();

  global $logged_in;
?>
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <title><?php echo $title; ?></title>
    <link type='text/css' rel='stylesheet' href='css/styles.css'>
  </head>
  <body>
    <header>
      <h1>Spock's blog</h1>
    </header>
    <section>
    <?php
    echo <<<_END
    
<article>
  <h3>$title</h3>
  <time datetime='$date'>$date</time>
  <hr>
  <p>
    $content
  </p>
  
_END;

    if ($logged_in) echo <<<_END
    
  <div class='menubuttons'>
    <a href='index.php?action=delete&amp;id=$id'>Delete post</a>
  </div>
  <div class='menubuttons'>
    <a href='index.php?action=edit&amp;id=$id'>Edit post</a>
  </div>
          
_END;

    echo "</article>";
    ?>
    </section>
    <footer>
      
    </footer>
  </body>
</html>
