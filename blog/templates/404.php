<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>404 Not Found</title>
    <link type='text/css' rel='stylesheet' href='css/styles.css'>
  </head>
  <body>
    <header>
      
    </header>
    <section>
      
      <article>
      <h2>404 Page not found</h2>

        <?php
          if ($cause)
          {
            echo "<p>Server said: $cause</p>";
          }
          if (isset($conn)) $conn->close();
        ?>
          
      </article>
      
    </section>
    <footer>
      
    </footer>
  </body>
</html>
