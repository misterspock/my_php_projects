<?php
  $result->data_seek(0);
  $row = $result->fetch_array(MYSQLI_ASSOC);
  extract($row);
  $result->close();
  $conn->close();
?>
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>Edit post</title>
    <link type='text/css' rel='stylesheet' href='css/styles.css'>
  </head>
  <body>
    <header>
      <h1>Spock's blog</h1>
    </header>
    <section>
    <?php
      echo <<<_END
      
<article>
  <form action='index.php' method='POST'>
    <input type='hidden' name='action'  value='editpost'>
    <input type='hidden' name='id'      value='$id'>

    <div class='tableRow'>
      <label>Title:</label>
      <input type='text' name='title' maxlength='255' value='$title' required='requred'>
    </div>
    <div class='tableRow'>
      <label>Summary info:</label>
      <textarea name='summary' rows='5' required='requred'>$summary</textarea>
    </div>
    <div class='tableRow'>
      <label>Content:</label>
      <textarea name='content' rows='15' required='requred'>$content</textarea>
    </div>
    
    <input type='submit' value="Save changes">
  </form>
</article>

_END;
    ?>
    </section>
    <footer>
      
    </footer>
  </body>
</html>
