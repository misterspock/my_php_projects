<?php
  global $logged_in;
?>
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>Spock's blog</title>
    <link type='text/css' rel='stylesheet' href='css/styles.css'>
  </head>
  <body>
    <header>
      <h1>Spock's blog</h1>
    </header>
    <section>
    <?php
      // Parsing DB output here.
      $rows = $result->num_rows;
      for ($i = $rows; $i > 0; --$i)
      {
        $result->data_seek($i - 1);
        $row = $result->fetch_array(MYSQLI_ASSOC);
        extract($row);
        // Now generate articles for each row of DB data.
        $summary = nl2br($summary); // This will translate all newline characters into HTML tags.
        echo <<<_END
        
<article>
  <h3><a href='index.php?action=viewpost&amp;id=$id'>
    $title
  </a></h3>
  <time datetime='$date'>$date</time><hr>
  <p>
    $summary
  </p>
</article>
          
_END;
      }

      // Closing result object.
      $result->close();
      $conn->close();

      if ($logged_in) echo <<<_END

<div class='menubuttons'>
  <a href='index.php?action=addpost'>New post</a>
</div>
<div class='menubuttons'>
  <a href='index.php?action=logout'>Log out</a>
</div>

_END;
      else echo <<<_END

<div class='menubuttons'>
  <a href='index.php?action=login'>Log in</a>
</div>

_END;
    ?>
    
    </section>
    <footer>
      
    </footer>
  </body>
</html>
