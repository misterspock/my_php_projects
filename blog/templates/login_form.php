<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>LogIn Form</title>
    <link type='text/css' rel='stylesheet' href='css/styles.css'>
  </head>
  <body>
    <header>
      <h1>Spock's blog</h1>
    </header>
    <section>
      <article id='loginform'>
        <form action='index.php' method='POST'>
          <input type='hidden' name='action' value='login'>
          
          <div class='tableRow'>
            <label>Login:</label>
            <input type='text' name='name' maxlength='64' required='requred'>
          </div>
          <div class='tableRow'>
            <label>Password:</label>
            <input type='password' name='password' maxlength='64' required='requred'>
          </div>
          
          <input type='submit' value="Log in">
        </form>
      </article>
    </section>
    <footer>
      
    </footer>
  </body>
</html>
