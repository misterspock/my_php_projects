<?php // model.php
  // The main model file.
  require_once 'core/login.php';
  
  // Connect to database.
  function connect()
  {
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if ($conn->connect_error) generate_404($conn->connect_error);
    
    return $conn;
  }
  
  // Select all articles.
  function get_all()
  {
    $conn = connect();
    
    $query = "SELECT * FROM articles";
    $result = $conn->query($query);
    
    if ($result) include 'templates/main_tmp.php';
    else generate_404($conn->error);
  }

  /*
  /  View chosen post on click. If parameter $edit is true the post
  /  will de delivered for edit. If parameter $delete if true the
  /  post will be deleted.
  */
  function get_by_id($id, $edit=false, $delete=false)
  {
    $conn = connect();

    $id = sanitize_mysql($conn, $id);

    if ($delete) $query = "DELETE FROM articles WHERE id=$id";
    else         $query = "SELECT * FROM articles WHERE id=$id";
    
    $result = $conn->query($query);
    if ($result)
    {
      if ($result->num_rows == 0) generate_404("No post with id=$id found in database!");
      else if ($delete)
      {
        header("Location: index.php");
        die();
      }
      else
      {
        if ($edit) include 'templates/editpost.php';
        else include 'templates/post.php';
      }
    }
    else generate_404($conn->error);
  }

  // Add new post to the database or update it.
  function add_or_update($id, $title, $summary, $content, $edit=false)
  {
    $conn = connect();

    $id      = sanitize_mysql($conn, $id);
    $title   = sanitize_mysql($conn, $title);
    $summary = sanitize_mysql($conn, $summary);
    $content = sanitize_mysql($conn, $content);
    
    $date = date('Y-m-d', time());
    
    if ($edit) $query = "UPDATE articles SET date='$date', title='$title', summary='$summary', content='$content' WHERE id=$id";
    else       $query = "INSERT INTO articles VALUES('$id', '$date', '$title', '$summary', '$content')";
    
    $result = $conn->query($query);
    if ($result)
    {
      header("Location: index.php");
      die();
    }
    else generate_404($conn->error);
  }

  // String sanitation for MySQL requests.
  function sanitize_mysql($conn, $str)
  {
    return $conn->real_escape_string($str);
  }

  // Generates 404 page.
  function generate_404($cause=null)
  {
    include 'templates/404.php';
  }

