function checkInput()
{
  var usernm = $('usernameErr').innerHTML;
  var passwd = $('passwdErr').innerHTML;
  var email  = $('emailErr').innerHTML;
  var age    = $('ageErr').innerHTML;

  if (usernm == "OK" &&
      passwd == "OK" &&
      email  == "OK" &&
      age    == "OK")
  {
    // alert("Okay");
    $('submButton').removeAttribute("disabled");
  }
  else
  {
    // alert("Not Okay");
    $('submButton').disabled = true;
  }
}

function validateUsername(field)
{
  if (field.length < 5)
  {
    $('usernameErr').innerHTML = "Username must be at least 5 characters"
  }
  else if (/[^a-zA-Z0-9_-]/.test(field))
  {
    $('usernameErr').innerHTML = "Only a-z, A-Z, 0-9, - and _ characters allowed in Usernames";
  }
  else
  {
    // Ajax username validation.
    var nocache = "&nocache=" + Math.random() * 1000000;
    var request = new XMLHttpRequest();
    request.open("GET", "validate.php?username=" + field + nocache, true);
    request.onreadystatechange = function ()
    {
      if (this.readyState == 4)
      {
        if (this.status == 200)
        {
          if (this.responseText != null)
          {
              $('usernameErr').innerHTML = this.responseText;
          }
          else alert("Ajax error: no data received");
        }
        else alert("Ajax error: " + this.statusText);
      }
    }
    request.send(null);
  }
}

function validatePasswd(field)
{
  if (field.length < 6)
  {
    $('passwdErr').innerHTML = "Password must be at least 6 characters"
  }
  else if (!(/[a-z]/.test(field)) || !(/[A-Z]/.test(field)) || !(/[0-9]/.test(field)))
  {
    $('passwdErr').innerHTML = "Passwords require one each of a-z, A-Z and 0-9";
  }
  else
  {
    $('passwdErr').innerHTML = "OK";
  }
}

function validateEmail(field)
{
  if (!(/[a-zA-Z0-9_\.-]+@[a-zA-Z0-9_\.-]+\.[a-zA-Z0-9_\.-]+/.test(field)))
  {
    $('emailErr').innerHTML = "Email is invalid.";
  }
  else
  {
    // Ajax email validation.
    var nocache = "&nocache=" + Math.random() * 1000000;
    var request = new XMLHttpRequest();
    request.open("GET", "validate.php?email=" + field + nocache, true);
    request.onreadystatechange = function ()
    {
      if (this.readyState == 4)
      {
        if (this.status == 200)
        {
          if (this.responseText != null)
          {
              $('emailErr').innerHTML = this.responseText;
          }
          else alert("Ajax error: no data received");
        }
        else alert("Ajax error: " + this.statusText);
      }
    }
    request.send(null);
  }
}

function validateAge(field)
{
  if (field < 18 || field > 110)
  {
    $('ageErr').innerHTML = "Age must be between 18 and 110";
  }
  else
  {
    $('ageErr').innerHTML = "OK";
  }
}

function $(id)
{
  return document.getElementById(id);
}
