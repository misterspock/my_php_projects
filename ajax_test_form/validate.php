<?php // validate.php
  // A PHP script for Username and Email Ajax validation.
  require_once 'login.php';

  $username = $email = "";

  // Get connected to the DB.
  $conn = new mysqli($hostnm, $usernm, $passwd, $dbase);
  if ($conn->connect_error) die($conn->connect_error);

  if (isset($_GET['username']))
  {
    $username = fix_string($_GET['username']);
    // Check DB for username existence.
    $query = "SELECT * FROM users WHERE username='$username'";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
    
    $rows = $result->num_rows;
    if ($rows != 0)
    {
      echo "This Username is already taken";
    }
    else
    {
      echo "OK";
    }
  }
  if (isset($_GET['email']))
  {
    $email = fix_string($_GET['email']);
    // Check DB for email existence.
    $query = "SELECT * FROM users WHERE email='$email'";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
    
    $rows = $result->num_rows;
    if ($rows != 0)
    {
      echo "This Email is already registered";
    }
    else
    {
      echo "OK";
    }
  }

  function fix_string($string)
  {
    if (get_magic_quotes_gpc()) $sting = stripslashes($string);
    return htmlentities($string);
  }
?>
