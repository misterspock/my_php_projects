<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>An Example Form</title>
    <style>
      body {
        font-family: Verdana, Helvetica, Arial, sans-serif;
        font-size:   small;
      }

      #table {
        display:     table;
        padding:     10px;
      }

      .tableRow {
        display:     table-row;
      }

      .left, .right {
        display:     table-cell;
      }

      .left {
        text-align:  right;
        font-weight: bold;
        padding-right: 10px;
      }
      
      #userform {
        width:       400px;
        padding:     10px;
        margin:      0px;
        background-color: #EEEEEE;
      }
    </style>
  </head>
  <body>
    <section id="userform">
      <h4>
        User was successfully added.
      </h4>
      <div id="table">
        <div class="tableRow">
          <p class="left">
            Forename:
          </p>
          <p class="right">
            %s
          </p>
        </div>
        <div class="tableRow">
          <p class="left">
            Surname:
          </p>
          <p class="right">
            %s
          </p>
        </div>
        <div class="tableRow">
          <p class="left">
            Username:
          </p>
          <p class="right">
            %s
          </p>
        </div>
        <div class="tableRow">
          <p class="left">
            Age:
          </p>
          <p class="right">
            %s
          </p>
        </div>
        <div class="tableRow">
          <p class="left">
            Email:
          </p>
          <p class="right">
            %s
          </p>
        </div>
      </div>
    </section>
  </body>
</html>
