<?php // adduser.php
  require_once 'login.php';

  $forename = $surname = $username = $password = $age = $email = "";

  $conn = new mysqli($hostnm, $usernm, $passwd, $dbase);
  if ($conn->connect_error) die($conn->connect_error);

  // Take user data and sanitize it.
  if (isset($_POST['forename']))
    $forename = fix_string($_POST['forename']);
  if (isset($_POST['surname']))
    $surname = fix_string($_POST['surname']);
  if (isset($_POST['username']))
    $username = fix_string($_POST['username']);
  if (isset($_POST['password']))
    $password = fix_string($_POST['password']);
  if (isset($_POST['age']))
    $age = fix_string($_POST['age']);
  if (isset($_POST['email']))
    $email = fix_string($_POST['email']);
  
  // Validate entered user data for consistency.
  $fail  = validate_forename($forename);
  $fail .= validate_surname($surname);
  $fail .= validate_username($username);
  $fail .= validate_password($password);
  $fail .= validate_age($age);
  $fail .= validate_email($email);

  // Check if anything failed.
  if ($fail == "")
  {
    // Add data to the database.
    $salt1 = "nDu&83%";
    $salt2 = "mKe(5#2";
    // Encrypt password.
    $token    = hash('ripemd128', "$salt1$password$salt2");
    // Make query string.
    $query = "INSERT INTO users VALUES('$forename', '$surname', '$username', '$token', '$age', '$email')";
    $result = $conn->query($query);
    if (!$result)
    {
      die($conn->error);
    }
    // Display added data.
    $answer = file_get_contents('success.tpl');
    printf($answer, $forename, $surname, $username, $age, $email);
  }
  else
  {
    // Output fail warning and the form.
    $answer = file_get_contents('form.tpl');
    printf($answer, $fail, $forename, $surname, $username, $password, $age, $email);
  }

  // Close connection to the database.
  $conn->close();
  
  // Validate entry functions.
  function validate_forename($field)
  {
    return (($field == "") ? "No Forename was entered.<br>" : "");
  }

  function validate_surname($field)
  {
    return (($field == "") ? "No Surname was entered.<br>" : "");
  }

  function validate_username($field)
  {
    if ($field == "") return "No Username was entered.<br>";
    else if (strlen($field) < 5) return "Username must be at least 5 characters.<br>";
    else if (preg_match("/[^a-zA-Z0-9_-]/", $field))
      return "Only a-z, A-Z, 0-9, - and _ characters allowed in Usernames.<br>";
    
    // Check if Username is already in the database.
    global $conn;
    $query = "SELECT * FROM users WHERE username='$field'";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
    
    $rows = $result->num_rows;
    if ($rows == 0)
    {
      $result->close();
      return "";
    }
    else
    {
      return "This Username already exist.<br>";
    }
  }

  function validate_password($field)
  {
    if ($field == "") return "No Password was entered.<br>";
    else if (strlen($field) < 6) return "Password must be at least 6 characters.<br>";
    else if (!preg_match("/[a-z]/", $field) ||
             !preg_match("/[A-Z]/", $field) ||
             !preg_match("/[0-9]/", $field))
      return "Password require one each of a-z, A-Z and 0-9.<br>";
    return "";
  }

  function validate_age($field)
  {
    if ($field == "") return "No Age was entered.<br>";
    else if ($field < 18 || $field > 110)
      return "Age must be between 18 and 110.<br>";
    return "";
  }

  function validate_email($field)
  {
    if ($field == "") return "No Email was entered.<br>";
    else if (!preg_match("/[a-zA-Z0-9_\.-]+@[a-zA-Z0-9_\.-]+\.[a-z]/", $field))
      return "The Email address is invalid.<br>";
    
    // Check if Email is already in the database.
    global $conn;
    $query = "SELECT * FROM users WHERE email='$field'";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
    
    $rows = $result->num_rows;
    if ($rows == 0)
    {
      $result->close();
      return "";
    }
    else
    {
      return "This Email already used.<br>";
    }
  }
  
  function fix_string($string)
  {
    if (get_magic_quotes_gpc()) $sting = stripslashes($string);
    return htmlentities($string);
  }
?>
