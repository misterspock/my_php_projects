<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title>Главная</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script src="js/jquery-2.1.4.js" type="text/javascript"></script>
  </head>
  <body>
    <?php include 'application/views/' . $content_view; ?>
  </body>
</html>