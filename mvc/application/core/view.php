<?php
  class View
  {
    // public $template_view; // Here we can define a default look of the page.
    
    public function generate($content_view, $template_view, $data=null)
    {
      /*
      if (is_array($data))
      {
        // If given data is in array form - extract it into variables.
        extract($data);
      }
      */
      
      include 'application/views/' . $template_view;
    }
  }