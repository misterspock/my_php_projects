<?php
  class Route
  {
    static function start()
    {
      // Default controller and action.
      $controller_name = 'Main';
      $action_name     = 'index';
      
      $routes = array();
      
      if (isset($_GET['url']))
      {
        $routes = explode('/', $_GET['url']);
      }
      
      // Get controller name.
      if (!empty($routes[0]))
      {
        $controller_name = $routes[0];
      }
      
      // Get action name.
      if (!empty($routes[1]))
      {
        $action_name = $routes[1];
      }
      
      // Add prefixes.
      $model_name      = 'Model_' . $controller_name;
      $controller_name = 'Controller_' . $controller_name;
      $action_name     = 'action_' . $action_name;
      
      // Hook up model class file (there can be no model file as well).
      $model_file = strtolower($model_name) . '.php';
      $model_path = "application/models/" . $model_file;
      
      if (file_exists($model_path))
      {
        include 'application/models/' . $model_file;
      }
      
      // Hook up controller class file.
      $controller_file = strtolower($controller_name) . '.php';
      $controller_path = "application/controllers/" . $controller_file;
      
      if (file_exists($controller_path))
      {
        include 'application/controllers/' . $controller_file;
      }
      else
      {
        // Throw exception or redirect to 404.
        Route::errorPage404();
      }
      
      // Instantiate controller.
      $controller = new $controller_name;
      $action     = $action_name;
      
      if (method_exists($controller, $action))
      {
        // Invoke controller action.
        $controller->{$action}();
      }
      else
      {
        // Throw exception or redirect to 404.
        Route::errorPage404();
      }
    }
    
    static function errorPage404()
    {
      $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
      header('HTTP/1.1 404 Not Found');
      header("Status: 404 Not Found");
      header('Location:' . $host . '404');
    }
  }